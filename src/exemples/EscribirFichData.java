package exemples;
import java.io.*;
public class EscribirFichData {
    public static void main(String[] args) throws IOException {
        File fichero = new File("FichData.dat");
        DataOutputStream dataOS = new DataOutputStream(new FileOutputStream(fichero));
        String[] nombres = {"Ana", "Luis Miguel", "Alicia"};
        int[] edades = {10, 20, 30};
        for (int i = 0; i < edades.length; i++) {
            dataOS.writeUTF(nombres[i]); //inserta nombre
            dataOS.writeInt(edades[i]); //inserta edad
        }
        dataOS.close(); //cerrar stream
    }
}