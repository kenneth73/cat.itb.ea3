import java.io.*;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) throws IOException {
        int inputUser;
        do {
            printMenu();
            System.out.println();
            inputUser = InputInt("Escull l'exercici a executar");
            switch (inputUser) {
                case 0:
                    break;
                case 1:
                    exercici1();
                    break;
                case 2:
                    exercici2();
                    break;
                case 3:
                    exercici3();
                    break;
                case 4:
                    mostraFitxer();
                    break;
                default:
                    System.out.println("ERROR");
                    break;
            }
        } while (inputUser != 0);
    }

    /*1.- Crear un fitxer binari per guardar dades de departaments, el nom del fitxer és "Departaments.dat".
    Introdueix com a mínim 10 departaments. Les dades per cada departament són:
        Número de departament: enter,
        Nom: String
        Localitat:String */
    public static void exercici1() throws IOException {
        File fichero = new File("Departaments.dat");
        DataOutputStream dataOS = new DataOutputStream(new FileOutputStream(fichero));
        int[] depId = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        String[] nombres = {
                "DBA", "System Admin", "Developers", "Customer Relationship", "Marketing",
                "Sales", "Purchasing", "Security", "Transport", "Management"};
        String[] localidades = {"Barcelona", "Berga", "Badalona", "Barbera del Valles","Santa Coloma de Gramanet",
                "Mongat","Tiana","Castelldefels","Gava","Sitges"};
        System.out.println("Dades (id, nom, localitat)");
        for (int i = 0; i < depId.length; i++) {
            dataOS.writeInt(depId[i]); //inserta depId
            dataOS.writeUTF(nombres[i]); //inserta nombres
            dataOS.writeUTF(localidades[i]); //inserta localidades
            System.out.println(depId[i]+ " " + nombres[i] + " "+ localidades[i]);
        }
        System.out.println("Les dades s'han guardat correctament al fitxer Departaments.dat");
        dataOS.close(); //cerrar stream
    }

    /*2.- Modificar les dades d'un departament. El mètode rep per teclat el número de departament a modificar, el nou
    nom de departament i la nova localitat. Si el departament no existeix, visualitza un missatge que ho indiqui.
    Visualitza també les dades antigues del departament i les noves dades.*/
    public static void exercici2() {
        File fileIN = new File("Departaments.dat");
        File fileOUT = new File("NewDepartaments");
        int id = InputInt("Introduce el ID de departamento a modificar : ");
        String nuevoDepartamento = InputAny("Introduce el nuevo nombre de departamento : ");
        String nuevaLocalidad = InputAny("Introduce la nueva localidad:");
        boolean existe = false;
        try {
            DataInputStream data = new DataInputStream(new FileInputStream(fileIN));
            DataOutputStream dataOUT = new DataOutputStream(new FileOutputStream(fileOUT));
            StringBuilder datosAntiguos = new StringBuilder();
            fileOUT.createNewFile();
            if (fileOUT.exists()) {
                while (data.available() > 1) {
                    int oldId = data.readInt();
                    if (oldId == id) {
                        datosAntiguos.append(oldId);
                        datosAntiguos.append(" || ").append(data.readUTF());
                        datosAntiguos.append(" || ").append(data.readUTF());
                        dataOUT.writeInt(oldId);
                        dataOUT.writeUTF(nuevoDepartamento);
                        dataOUT.writeUTF(nuevaLocalidad);
                        existe = true;
                    } else {
                        dataOUT.writeInt(oldId);
                        dataOUT.writeUTF(data.readUTF());
                        dataOUT.writeUTF(data.readUTF());
                    }
                }
                if (existe) {
                    dataOUT.close();
                    data.close();
                    File file = new File(fileIN.getName());
                    fileIN.delete();
                    fileOUT.renameTo(file);
                    System.out.println("Departamento Antiguo : " + datosAntiguos);
                    System.out.println("Departamento Nuevo: " + id + " || " + nuevoDepartamento + " || " + nuevaLocalidad);
                } else {
                    fileOUT.delete();
                    System.out.println("No se ha encontrado el id");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*3.- Eliminar físicament un departament. El mètode rep pel teclat el número de departament a eliminar. Si el
    departament no existeix, visualitza un missatge que ho indiqui. Visualitza també el número total de
    departaments que existeixen en el fitxer.*/
    public static void exercici3() {
        File fileIN = new File("Departaments.dat");
        File fileOUT = new File("NewDepartaments.dat");
        int id = InputInt("Introdueix el ID del departament a borrar : ");
        boolean existeix = false;
        try {
            DataInputStream dataIN = new DataInputStream(new FileInputStream(fileIN));
            DataOutputStream dataOUT = new DataOutputStream(new FileOutputStream(fileOUT));

            fileOUT.createNewFile();
            if (fileOUT.exists()) {
                while (dataIN.available() > 1 && !existeix) {
                    int idf = dataIN.readInt();
                    if (idf == id) {
                        dataIN.readUTF();
                        dataIN.readUTF();
                        existeix = true;

                    } else {
                        dataOUT.writeInt(idf);
                        dataOUT.writeUTF(dataIN.readUTF());
                        dataOUT.writeUTF(dataIN.readUTF());
                    }
                }
                if (!existeix) {
                    System.out.println("No s'ha trobat el departament amb id = " + id);
                } else {
                    System.out.println("S'ha borrat correctament el departament amb id =" + id);
                }
                dataOUT.close();
                dataIN.close();
                File file = new File(fileIN.getName());
                fileIN.delete();
                fileOUT.renameTo(file);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void mostraFitxer() {
        try (DataInputStream data = new DataInputStream(new FileInputStream("Departaments.dat"))) {
            while (data.available() > 1) {
                System.out.println(data.readInt() + "\t" + data.readUTF() + "\t" + data.readUTF());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void printMenu() {
        System.out.println("\nMenu Principal");
        System.out.println("[1]. Ex1(Insertar)" + "\n[2]. Ex2(Modificar)" + "\n[3]. Ex3(Borrar)" + "\n[4]. Mostrar Fitxer" );
    }

    public static int InputInt(String m){
        Scanner scan = new Scanner(System.in);
        boolean correct;
        int num = 0;
        do{
            System.out.println(m);
            if(correct = scan.hasNextInt()){
                num = scan.nextInt();
            }else{
                scan.nextLine();
            }
        }while(!correct || num <=0);
        return num;
    }

    public static String InputAny(String m){
        Scanner scan = new Scanner(System.in);
        System.out.println(m);
        return scan.nextLine();
    }

}
